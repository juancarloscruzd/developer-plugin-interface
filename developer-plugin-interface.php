<?php
/*
Plugin Name: Developer Plugin Interface
Plugin URI: http://www.petersenmediagroup.com/plugins/
Description: Adds functionality plugin via a plugin shell. Give your functions.php a rest.
Version: 1.0.0
Author: Jesse Petersen
Author URI: http://www.petersenmediagroup.com

	Copyright 2013 Jesse Petersen

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as
	published by the Free Software Foundation.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Start up the engine
class Developer_Interface_Plugin
{

	/**
	 * Static property to hold our singleton instance
	 *
	 * @return Developer_Interface_Plugin
	 */
	static $instance = false;

	/**
	 * This is our constructor
	 *
	 * @return Developer_Interface_Plugin
	 */
	private function __construct() {
		add_filter		( 'comment_form_defaults',		array( $this, 'remove_notes_filter'		) 			);
	}

	/**
	 * If an instance exists, this returns it.  If not, it creates one and
	 * retuns it.
	 *
	 * @return Developer_Interface_Plugin
	 */

	public static function getInstance() {
		if ( !self::$instance )
			self::$instance = new self;
		return self::$instance;
	}

	/**
	 * The actual filter for removing the notes.
	 *
	 * @return Developer_Interface_Plugin
	 */

	public function remove_notes_filter($fields) {

		$fields['comment_notes_after'] = '';
		return $fields;
	}

/// end class
}


// Instantiate our class
$Developer_Interface_Plugin = Developer_Interface_Plugin::getInstance();
